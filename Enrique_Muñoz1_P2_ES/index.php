
<?php include("template/cabecera.php"); ?>

     <div class="jumbotron text-center">
         <div class="row">
            <div class="jumbotron">
            <?php if(isset($_SESSION['nick'])){ ?>
                <h1 class="display-3">Bienvenido <?php echo  $_SESSION['nick']?></h1>
             <?php  } ?>
             <?php if(!isset($_SESSION['nick'])){ ?>
                <h1 class="display-3">Bienvenidos a Recetas para Vicente</h1></h1>
             <?php  } ?>
                <p class="lead">Podrás encontrar recetas tradicionales, slow Food y recetas de Freidoras sin aceite.</p>
                <hr class="my-2">
                <br/>
                <img width="700" src="img/img1.jpg"  class="img-thumbnail rounded mx-auto d-block" >
                <p class="lead">
                <br/>
                    <a class="btn btn-primary btn-lg" href="recetas.php" role="button">Ver todas las recetas</a>
                </p>
            </div>
        </div>
    </div>

 <?php include("template/pie.php"); ?>
