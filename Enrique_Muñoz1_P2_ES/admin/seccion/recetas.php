<?php include("../template/cabecera.php"); ?>
<?php


$txtID=(isset($_POST['txtID']))?$_POST['txtID']:"";
$txtNombre=(isset($_POST['txtNombre']))?$_POST['txtNombre']:"";
$txtIngredientes=(isset($_POST['txtIngredientes']))?$_POST['txtIngredientes']:"";
$txtElaboracion=(isset($_POST['txtElaboracion']))?$_POST['txtElaboracion']:"";
$txtImg=(isset($_FILES['txtImg']['name']))?$_FILES['txtImg']['name']:"";
$accion=(isset($_POST['accion']))?$_POST['accion']:"";

include("../config/bd.php");

switch($accion){

    case "Agregar":
        $sentenciaSQL=$conexion->prepare("INSERT INTO recetas (nombre, ingredientes, imagen, elaboracion) VALUES (:nombre, :ingredientes, :imagen, :elaboracion);");
        $sentenciaSQL->bindParam(':nombre', $txtNombre);
        $sentenciaSQL->bindParam(':ingredientes', $txtIngredientes);
        $sentenciaSQL->bindParam(':elaboracion', $txtElaboracion);

        $fecha= new DateTime();
        $nombreArchivo=($txtImg!="")?$fecha->getTimestamp()."_".$_FILES["txtImg"]["name"]:"imagen.jpg";

        $tmpImagen=$_FILES["txtImg"]["tmp_name"];

        if($tmpImagen!=""){
            move_uploaded_file($tmpImagen,"../../img/".$nombreArchivo);
        }

        $sentenciaSQL->bindParam(':imagen', $nombreArchivo);
        $sentenciaSQL->execute();

        header("Location:recetas.php");
        break;
        
    case "Modificar":

        $sentenciaSQL=$conexion->prepare("UPDATE recetas SET nombre=:nombre WHERE id=:id");
        $sentenciaSQL->bindParam(':nombre', $txtNombre);
        $sentenciaSQL->bindParam(':id', $txtID);
        $sentenciaSQL->execute();

        $sentenciaSQL=$conexion->prepare("UPDATE recetas SET ingredientes=:ingredientes WHERE id=:id");
        $sentenciaSQL->bindParam(':ingredientes', $txtIngredientes);
        $sentenciaSQL->bindParam(':id', $txtID);
        $sentenciaSQL->execute();

        $sentenciaSQL=$conexion->prepare("UPDATE recetas SET elaboracion=:elaboracion WHERE id=:id");
        $sentenciaSQL->bindParam(':elaboracion', $txtElaboracion);
        $sentenciaSQL->bindParam(':id', $txtID);
        $sentenciaSQL->execute();

        if($txtImg!=""){
            $fecha= new DateTime();
            $nombreArchivo=($txtImg!="")?$fecha->getTimestamp()."_".$_FILES["txtImg"]["name"]:"imagen.jpg";
            $tmpImagen=$_FILES["txtImg"]["tmp_name"];

            move_uploaded_file($tmpImagen,"../../img/".$nombreArchivo);

            $sentenciaSQL=$conexion->prepare("SELECT imagen FROM recetas WHERE id=:id");
            $sentenciaSQL->bindParam(':id', $txtID);
            $sentenciaSQL->execute();
            $receta=$sentenciaSQL->fetch(PDO::FETCH_LAZY);
    
            if( isset($receta["imagen"]) && ($receta["imagen"]!="imagen.jpg") ){
                if(file_exists("../../img/".$receta["imagen"])){
    
                    unlink("../../img/".$receta["imagen"]);
                }
            }

            $sentenciaSQL=$conexion->prepare("UPDATE recetas SET imagen=:imagen WHERE id=:id");
            $sentenciaSQL->bindParam(':imagen', $nombreArchivo);
            $sentenciaSQL->bindParam(':id', $txtID);
            $sentenciaSQL->execute();
        }

        header("Location:recetas.php");
        break;

    case "Cancelar":
            header("Location:recetas.php");
        break;

    case "Seleccionar":

        $sentenciaSQL=$conexion->prepare("SELECT * FROM recetas WHERE id=:id");
        $sentenciaSQL->bindParam(':id', $txtID);
        $sentenciaSQL->execute();
        $receta=$sentenciaSQL->fetch(PDO::FETCH_LAZY);

        $txtNombre=$receta['nombre'];
        $txtIngredientes=$receta['ingredientes'];
        $txtImg=$receta['imagen'];
        $txtElaboracion=$receta['elaboracion'];
      
        break;

    case "Borrar":

        $sentenciaSQL=$conexion->prepare("SELECT imagen FROM recetas WHERE id=:id");
        $sentenciaSQL->bindParam(':id', $txtID);
        $sentenciaSQL->execute();
        $receta=$sentenciaSQL->fetch(PDO::FETCH_LAZY);

        if( isset($receta["imagen"]) && ($receta["imagen"]!="imagen.jpg") ){
            if(file_exists("../../img/".$receta["imagen"])){

                unlink("../../img/".$receta["imagen"]);
            }
        }

        $sentenciaSQL=$conexion->prepare("DELETE FROM recetas WHERE id=:id");
        $sentenciaSQL->bindParam(':id', $txtID);
        $sentenciaSQL->execute();

        header("Location:recetas.php");
      
        break;

}   
$sentenciaSQL=$conexion->prepare("SELECT * FROM recetas");
$sentenciaSQL->execute();
$listaRecetas=$sentenciaSQL->fetchAll(PDO::FETCH_ASSOC);



?>
<div class="col-md-12">

<div class="card">
    <div class="card-header">
        Datos de receta
    </div>

    <div class="card-body">
    <form method="POST" enctype="multipart/form-data">

<div class = "form-group">
<label for="txtID">ID:</label>
<input type="text" required readonly class="form-control" value="<?php echo $txtID; ?>" name="txtID" id="txtID"  placeholder="ID">
</div>

<div class = "form-group">
<label for="txtNombre">Nombre:</label>
<input type="text" required class="form-control" value="<?php echo $txtNombre; ?>" name="txtNombre" id="txNombre"  placeholder="Nombre de la receta">
</div>

<div class = "form-group">
<label for="txtIngredientes">Ingredientes:</label>
<textarea required class="form-control" name="txtIngredientes" id="txtIngredientes"  placeholder="Ingredientes..." rows="3"><?php  echo $txtIngredientes; ?></textarea>
</div>

<div class = "form-group">
<label for="txtElaboracion">Elaboración:</label>
<textarea required class="form-control" name="txtElaboracion" id="txtElaboracion"  placeholder="Elaboración..." rows="3"><?php echo $txtElaboracion; ?></textarea>
</div>

<div class = "form-group">
<label for="txtImg">Imagen:</label>

<br/>

<?php

if($txtImg!=""){ ?>

    <img class="img-thumbnail rounded"src="../../img/<?php echo $txtImg;?>" width="150" alt="" srcset="">


<?php } ?>


<input type="file" class="form-control" name="txtImg" id="txImg"  placeholder="Imagen de la receta">

</div>


<div class="btn-group" role="group" aria-label="">
    <button type="submit" name="accion" <?php echo ($accion=="Seleccionar")?"disabled":"";?> value="Agregar" class="btn btn-success">Agregar</button>
    <button type="submit" name="accion" <?php echo ($accion!="Seleccionar")?"disabled":"";?> value="Modificar" class="btn btn-warning">Modificar</button>
    <button type="submit" name="accion" <?php echo ($accion!="Seleccionar")?"disabled":"";?> value="Cancelar" class="btn btn-info">Cancelar</button>
</div>
</form>
    </div>

    
</div>

    
    
    
</div>

<div class="col-md-12">
    <table class="table table-bordered" id="tabla">
    <br/>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Ingredientes</th>
                <th>Elaboración</th>
                <th>Imagen</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($listaRecetas as $receta) { ?>
            <tr>
                <td><?php echo $receta['id']; ?></td>
                <td><?php echo $receta['nombre']; ?></td>
                <td><?php echo $receta['ingredientes']; ?></td>
                <td><?php echo $receta['elaboracion']; ?></td>
                <td><img class="img-thumbnail rounded" src="../../img/<?php echo $receta['imagen']; ?>" width="100" alt="" srcset=""></td>
                <td>
                <form  method="post">
                <input type="hidden" name="txtID" id="txtID" value="<?php echo $receta['id']; ?>" />
                <input type="submit" name="accion" value="Seleccionar" class="btn btn-primary"/>
                <input type="submit" name="accion" value="Borrar" class="btn btn-danger"/>
                </form>
                </td>
            </tr>
           <?php } ?> 
        </tbody>
    </table>
</div>

<script>

    var tabla= document.querySelector("#tabla");

    var dataTable = new DataTable(tabla,{
        perPage:4,
        perPageSelect:[4,8,12,16,20],
        labels: {
        placeholder: "Buscar:",
        perPage: "{select} Registros por pagina",
        noRows: "Registro no Encontrado",
        info: "Mostrando registros del {start} al {end} de {rows}",
        }
    });

</script>

<?php include("../template/pie.php");?>