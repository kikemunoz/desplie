<?php include("template/cabecera.php"); ?>

<div class="container">
      <div class="row">
      <div class="col-md-4">    
      </div>
          <div class="col-md-4">
          <br/><br/>           
          <div class="card">
              <div class="card-header">
                  Mi perfil
              </div>
              <div class="card-body">            
                 <form method="POST" action="modificarPerfil.php" >
                 <div class = "form-group">
                 <label>Nickname:</label>
                 <input required type="text" class="form-control" name="nickname"  id="nickname" value="<?php echo  $_SESSION['nick']?>">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Nombre:</label>
                 <input required  type="text" class="form-control" name="nombre"  id="nombre" value="<?php echo  $_SESSION['nombre']?>">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Primer apellido:</label>
                 <input required type="text" class="form-control" name="apellido1"  id="apellido1" value="<?php echo  $_SESSION['apellido1']?>">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Segundo apellido:</label>
                 <input required type="text" class="form-control" name="apellido2"  id="apellido2" value="<?php echo  $_SESSION['apellido2']?>">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Edad:</label>
                 <input required type="text" class="form-control" name="edad"  id="edad" value="<?php echo  $_SESSION['edad']?>">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Telefono:</label>
                 <input required type="text" class="form-control" name="telefono"  id="telefono" value="<?php echo  $_SESSION['telefono']?>">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Email:</label>
                 <input required type="email" class="form-control" name="correo"  id="email" value="<?php echo  $_SESSION['email']?>">
                 </div>
                 <br/>
                 <div style="text-align: center;">
                 <br/>
                 <a href="contrasena.php">Cambiar contraseña</a>
                 <br/><br/>
                 <button type="submit" name="" class="btn btn-primary">Modificar</button>
                 <br/>
                </div>
                 </form>  
              </div>   
            </div>
          </div>     
      </div>
  </div>
</p>
  <?php include("template/pie.php"); ?>
