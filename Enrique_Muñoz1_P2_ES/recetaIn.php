<?php include("template/cabecera.php"); ?>

<?php 

include ("admin/config/bd.php");



        if(isset($_GET)){
            $id = $_GET['id'];
            $sentenciaSQL=$conexion->prepare("SELECT * FROM recetas WHERE id=:id");
            $sentenciaSQL->bindParam(':id', $id);
            $sentenciaSQL->execute();
            $receta=$sentenciaSQL->fetch(PDO::FETCH_LAZY);

            $txtNombre=$receta['nombre'];
            $txtInfo=$receta['ingredientes'];
            $txtImg=$receta['imagen'];
            $txtInfo=$receta['elaboracion'];

            $sentencia=$conexion->prepare("SELECT * FROM comentarios WHERE id_receta=:id");
            $sentencia->bindParam(':id', $id);
            $sentencia->execute();
            $listaComentarios=$sentencia->fetchAll(PDO::FETCH_ASSOC);


            
           
            
        }
    
?>
<div class="col-lg-6" style="float:none;margin:auto;"> 
<h2 class="card-title" style="text-align: center;"><?php echo $receta['nombre']; ?></h2> 
<div class="card">
<img width="auto" class="img-responsive" src="./img/<?php echo $receta['imagen']; ?>" alt="">
<div class="card-body">
    
    <br/>
    <h5>Ingredientes:</h5>
    <?= nl2br($receta['ingredientes']) ?>
    <br/><br/>
    <h5>Elaboración:</h5>
    <?= nl2br($receta['elaboracion']) ?>
    
</div>
</div>
    </br>
    <?php if (isset ($_SESSION['nick'])){ ?>

    
    <h4 class="card-title">Escribe tu comentario</h4> 
    <div class="form-group">
    <form method="POST" action="comentarios.php" >
    <textarea class="form-control" name="comentario" id="" rows="3"></textarea>
    <input type="hidden" name="id_receta" value="<?=$receta['id'];?>">
    </div>
    <br/>
    <div>
    <button type="submit" name="login" class="btn btn-primary">Comentar</button>
    </div>
    </form>

    <?php } else { ?>
        <h5 class="card-title">¡Registrate para poder comentar!</h5> 
    
    <?php } ?>
    
    <br/>
    
    <?php foreach($listaComentarios as $comentario) { 
            $senten=$conexion->prepare("SELECT * FROM usuarios WHERE id=:id");
            $senten->bindParam(':id', $comentario['id_usuario']);
            $senten->execute();
            $usuario=$senten->fetch(PDO::FETCH_LAZY);
            ?>
    <div class="col-lg-12">    
    <div class="card">
    <p>Usuario <?php echo $usuario['nick']; ?>  : <?php echo $comentario['comentario']; ?></p>
    </div>
    </div>

    <?php } ?>
<br/>
  

<div style="text-align: center;">
<a href="recetas.php">
<button type="button"  class="btn btn-primary" href="recetas.php">Volver a recetas</button>
</a>

        <br/>
    <br/>
    </div>




<?php include("template/pie.php"); ?>






