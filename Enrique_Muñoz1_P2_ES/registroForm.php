<?php include("template/cabecera.php"); ?>

<div class="container">
      <div class="row">
      <div class="col-md-4">    
      </div>
          <div class="col-md-4">
          <br/><br/>           
          <div class="card">
              <div class="card-header">
                  Registro
              </div>
              <div class="card-body">            
                 <form method="POST" action="registro.php" >
                 <div class = "form-group">
                 <label>Nickname:</label>
                 <input required type="text" class="form-control" name="nickname"  id="nickname" placeholder="Introduce tu nickname">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Nombre:</label>
                 <input required  type="text" class="form-control" name="nombre"  id="nombre" placeholder="Introduce tu nombre">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Primer apellido:</label>
                 <input required type="text" class="form-control" name="apellido1"  id="apellido1" placeholder="Introduce tu primer apellido">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Segundo apellido:</label>
                 <input required type="text" class="form-control" name="apellido2"  id="apellido2" placeholder="Introduce tu segundo apellido">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Contraseña:</label>
                 <input required type="password" class="form-control" name="password"  id="contrasena" placeholder="Introduce contraseña">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Edad:</label>
                 <input required type="text" class="form-control" name="edad"  id="edad" placeholder="Introduce tu edad">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Telefono:</label>
                 <input required type="text" class="form-control" name="telefono"  id="telefono" placeholder="Introduce tu teléfono">
                 </div>
                 <br/>
                 <div class = "form-group">
                 <label>Email:</label>
                 <input required type="email" class="form-control" name="correo"  id="email" placeholder="Introduce email">
                 </div>
                 <br/>
                 <div style="text-align: center;">
                 <button type="submit" name="login"  class="btn btn-primary">Registrarse</button>
                 <br/>
                
	             </p>
                ¿Ya estás registrado?
				          <a href="loginForm.php" class="to_register"> Ingresar </a>
				        </p>
                </div>
                 </form>  
              </div>   
            </div>
          </div>     
      </div>
  </div>
</p>
  <?php include("template/pie.php"); ?>
