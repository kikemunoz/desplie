<?php

session_start();

$host_db = "localhost";
$user_db = "root";
$pass_db = "";
$db_name = "sitioweb";
$tbl_name = "usuarios";

$conexion = new mysqli($host_db, $user_db, $pass_db, $db_name);

if ($conexion->connect_error) {
 die("Conexión fallida " . $conexion->connect_error);
}

if(isset($_POST)){
    $nickname = $_POST['nickname']; 
    $password = $_POST['password'];


    $stmt = $conexion->prepare("SELECT * FROM $tbl_name WHERE nick=? and contrasena=?");
    $stmt->bind_param('ss',$nickname,$password);
    $stmt->execute();
    $resultado = $stmt->get_result();

    if ($resultado->num_rows > 0) { 

        $row=$resultado-> fetch_array(MYSQLI_ASSOC);
        $_SESSION['nick']= $row['nick'];
        $_SESSION['nombre']= $row['nombre'];
        $_SESSION['apellido1']= $row['apellido1'];
        $_SESSION['apellido2']= $row['apellido2'];
        $_SESSION['contrasena']= $row['contrasena'];
        $_SESSION['edad']= $row['edad'];
        $_SESSION['telefono']= $row['telefono'];
        $_SESSION['email']= $row['email'];
        $_SESSION['id']= $row['id'];
 
        header('Location:index.php');
    } else {
        
        header('Location:loginForm.php?error=si');

    }
    
}


 mysqli_close($conexion); 



?>
