-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2021 a las 22:43:11
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sitioweb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id_comentario` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_receta` int(11) NOT NULL,
  `comentario` varchar(2500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id_comentario`, `id_usuario`, `id_receta`, `comentario`) VALUES
(11, 34, 41, 'que rico!!!'),
(12, 34, 39, '@kikote eso lo diras tu!'),
(13, 34, 49, 'que rico! :)'),
(14, 36, 39, 'Para hacer una paella no hace falta ser valenciano!'),
(15, 36, 46, 'que ricooo!'),
(16, 36, 54, 'estupenda!'),
(17, 37, 48, 'pero que ricos'),
(18, 38, 47, 'que buenos! una pasada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas`
--

CREATE TABLE `recetas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `ingredientes` varchar(2000) NOT NULL,
  `imagen` varchar(1000) NOT NULL,
  `elaboracion` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `recetas`
--

INSERT INTO `recetas` (`id`, `nombre`, `ingredientes`, `imagen`, `elaboracion`) VALUES
(39, 'Paella valenciana', 'Para 12 personas\r\n   - Arroz bomba 1500\r\n   - Pollo de corral 1\r\n   - Conejo 0.5\r\n   - Judía verde plana 500 g\r\n   - Garrofó 500 g\r\n   - Alcachofa opcional 6\r\n   - Caracoles 500 g\r\n   - Aceite de oliva virgen extra\r\n   - Pimentón dulce\r\n   - Tomate triturado\r\n   - Azafrán\r\n   - Romero fresco\r\n   - Sal\r\n', '1637088549_paella.jpg', 'Toda paella que se precie comienza por un buen sofrito. En una paella cuanto más grande mejor, se sofríe en abundante aceite el pollo, el conejo, las judías, las alcachofas y los caracoles, sazonando con un poco de sal y pimentón hacia el final. Cuando esté bien dorado se añade el tomate triturado y se rehoga.\r\n\r\nCon el sofrito listo se debe de añadir el agua. Es un buen momento de echar el azafrán o, en su defecto, el sazonador de paella, que lleva sal, ajo, colorante y un poco de azafrán. En una paella cuanto más grande mejor, se sofríe en abundante aceite el pollo, el conejo, las judías, las alcachofas y los caracoles, sazonando con un poco de sal y pimentón hacia el final. Cuando esté bien dorado se añade el tomate triturado y se rehoga.\r\n\r\nLuego añadimos el arroz y lo distribuimos por la paella. Cocemos entre 17 y 20 minutos.  A mitad cocción también podemos poner unas ramitas de romero, que retiraremos antes de servir.\r\n\r\nPor último, conviene dejar la paella reposar unos minutos tapada con un gran paño o papel de periódico -no es bueno porque con la humedad se puede liberar algo de tinta, pero toda la vida lo he visto usar- antes de servirla y recibir el aplauso de los presentes.'),
(40, 'Zarangollo murciano', 'Para 4 personas \r\n- 2 cebolletas\r\n- 3 o 4 calabacines\r\n- 3 huevos \r\n- Aceite de oliva virgen extra \r\n- Sal\r\n- Pimienta negra molida', '1637087690_zarangollo.jpg', 'Pelar la cebolleta y cortarla en juliana. Pelar los calabacines y cortar en discos finos. Sofreír la cebolleta con una pizca de sal y dejar pochar, añadir después el calabacín y salpimentar ligeramente. \r\n\r\nCocinar a fuego fuerte para que vaya soltando el agua. Cuando el calabacín no esté tan crudo, bajar el fuego y cocinar lentamente, Remover de vez en cuando hasta que esté muy tierno. \r\n\r\nPor último, cascar los huevos encima, salpimentar y remover suavemente, rompiendo las yemas, dejándolos poco cuajados y jugosos pero no caldosos. Reposar fuera del fuego y servir. '),
(41, 'Gazpacho andaluz', 'Para 6 personas \r\n- 1kg de tomate pera\r\n- 1 pimiento verde italiano \r\n- 1 pepino \r\n- 2 dientes de ajo\r\n- 50ml de aceite de oliva virgen extra\r\n- 50g de pan de hogaza duro\r\n- 250ml de agua\r\n- 5g de sal\r\n- 30ml de vinagre de Jerez', '1637088164_gazpacho.jpg', 'Troceamos los ingredientes y los añadimos junto a los líquidos en un vaso batidor o en la Thermomix. \r\n\r\nTrituramos bien, incluso con pieles, porque luego pasaremos el resultado por un colador fino. \r\n\r\nMetemos en la nevera para que enfríe y a disfrutar.'),
(42, 'Pisto manchego', 'Para 4 personas\r\n- 2 dientes de ajo\r\n- 250g de cebolla\r\n- 200g de pimiento verde\r\n- 200g de pimiento rojo\r\n- 4 tomates\r\n- 300g de calabacín\r\n- Sal\r\n- Pimienta negra molida\r\n- Aceite de oliva virgen extra', '1637088490_pisto.jpg', 'Llevamos a ebullición agua para pelar los tomates. Retiramos la parte dura y hacemos un corte de cruz en la base. Los introducimos en el agua durante 15-20 segundos, sacamos y llevamos a bol con agua helada. Los pelamos y trituramos. \r\n\r\nDespués pelamos y picamos finamente la cebolla y el ajo. Además, lavamos el resto de verduras y la troceamos en igual tamaño. Calentamos una buena cantidad de aceite en la cazuela y pochamos ajo y cebolla a fuego suave durante 15 minutos. \r\n\r\nAñadimos los pimientos y pochamos otros 15 minutos. Por último, añadimos calabacín y tomate, salpimentamos, tapamos, dejando pochar durante una hora y media. Por último, retiramos la tapa, subimos el fuego y cocecmos hasta que el agua del tomate se haya evaporado. Debe quedar jugoso pero sin restos de agua.'),
(43, 'Boniato freidora de aire', '   - Boniatos, 2 pequeños\r\n   - Aceite de oliva virgen extra, 1 cucharada\r\n   - Ajo en polvo, !/2 cucharadita\r\n   - Pimentón dulce o picante o mezcla de ambos, 1/2 cucharadita\r\n   - Sal, al gusto\r\n   - Pimienta molida, al gusto\r\n   - Fécula de maíz, 1 cucharadita\r\n\r\n', '1637089133_boniato.jpg', 'Pelamos los boniatos, los cortamos en bastones como de medio centímetro y los colocamos en un bol, los rociamos con el aceite y agitamos un poco para que se impregnen con el aceite.\r\n\r\nEn una taza mezclamos el ajo en polvo, el pimentón, la sal, la pimienta y la fécula de maíz. Echamos la mezcla sobre los boniatos, removemos para que queden bien sazonados con la mezcla.\r\n\r\nPrecalentamos la freidora de aire caliente a 180 ºC y ponemos una capa de boniatos intentando que no queden muy amontonados. Cocinamos durante unos 8 minutos, abrimos, agitamos la cesta de la freidora para que se volteen los trozos de boniato. Seguimos cocinando durante otros 8 o 10 minutos. Pasado este tiempo, comprobamos si están ya tiernas por dentro y crujientes por fuera y, si es necesario, las dejamos algunos minutos más.'),
(44, 'Curry de sandía', 'Para dos personas\r\n    - Sandía, 1/2\r\n    - Aceite vegetal, 2 cucharadas\r\n    - Cominos enteros, 1/4 cucharadita\r\n    - Curry de Madrás, 1 cucharada (*)\r\n    - Lima, 1\r\n    - Chips de coco, 30 g\r\n    - Sal\r\n    - Cilantro fresco o hierbabuena fresca\r\n\r\n', '1637089727_sandia.jpg', 'En una cazuela ponemos a calentar el aceite a fuego medio-bajo con los cominos hasta que empiecen a desprender su aroma. Trituramos la cuarta parte de la sandía y la mezclamos con la mezcla de especias de curry de Madrás. Añadimos la mezcla del paso anterior a la cazuela y cocinamos a fuego medio removiendo de vez en cuando hasta que se reduzca más o menos a la mitad.\r\n\r\nAñadimos el zumo de media lima y los chips de coco deshidratado. Damos unas vueltas para mezclarlo todo, pero sin que se cocine demasiado pues no queremos que el coco se ablande en exceso, sino que siga manteniendo ese puntito crujiente. Añadimos el resto de la sandía cortada en cubos, mezclamos bien para que se impregne de la salsa de especias y dejamos a fuego bajo durante unos cinco minutos. Probamos y, si es necesario, rectificamos de sal.\r\n\r\nServimos inmediatamente sobre una cama de arroz basmati, rociamos con unas gotas de zumo de lima y ponemos por encima algunas hojas de cilantro fresco o hierbabuena.'),
(45, 'Verduras freidora de aire', '   - Zanahorias\r\n   - Remolacha\r\n   - Endibias\r\n   - Hojas de romero\r\n   - Sal Maldon\r\n   - Aceite de oliva\r\n', '1637090072_verduras.jpg', 'En primer lugar, pelamos y cortamos en láminas finas las zanahorias y la remolacha. En un cazo con agua, hervimos las zanahorias y la remolacha durante 3 minutos. Por otro lado, lavamos y abrimos las endibias. Reservamos. A continuación, encendemos la freidora a 180ºC.\r\n\r\nEn una bandeja, colocamos por tipos las zanahorias, la remolacha y las endibias. Salamos con sal Maldon y echamos un chorrito de aceite de oliva. Coronamos con unas hojas de romero.\r\n\r\nA 180ºC durante 8-10 minutos. Emplatamos las verduras por tipos y servimos.\r\n'),
(46, 'Crema de avena y chufa', 'Para 2 personas\r\n   - 2 calabaza mediana tipo cacahuete\r\n   - 2 lata leche de coco nata de coco mínimo 80%\r\n   - 900 ml agua filtrada\r\n   - 8 cucharadas harina de chufa\r\n   - 2 cucharadas maca andina en polvo\r\n   - 12 cucharadas copos de avena sin gluten\r\n   - 2 cucharadita curry dulce\r\n   - 2 diente ajo negro\r\n   - 2 manzana\r\n\r\nPara el topping:\r\n   - 4 cucharaditas copos de avena sin gluten\r\n   - 2 cucharadas semillas de lino\r\n   - 4 cucharaditas leche de coco\r\n   - 24 nueces pecan\r\n   - 4 cucharaditas sirope de yacón', '1637090851_crema.jpg', 'Pelar la calabaza, y quitar las semillas, cortar en trozos. Si es ecológica es recomendable lavarla bien con agua y jabón eco, tipo Marsella. Después reservar las semillas o pepitas y la piel para hacer la sopa más completa. Con la manzana hacer lo mismo.\r\n\r\nEchar los trozos de la calabaza y manzana en la batidora de alta potencia junto con los demás ingredientes, menos las nueces de pecan, el sirope de yacon, la avena para el topping y un par de cucharaditas de leche de coco para la decoración. Poner la Vitamix a máxima potencia durante 7 minutos, y mientras preparar el toping de avena “caramelizada”.\r\n\r\nPara el topping también se puede usar la batidora Personal Blender, tiene diferentes vasos para hacer diferentes salsas y muele perfectamente las semillas. Poner en uno de sus vasos, con las aspas más planas, los copos de avena sin gluten junto al sirope de yacon y las semillas de lino.\r\nDar varios toques, tipo molinillo de café, hasta caramelizar y conseguir una textura como de granola. Reservar.\r\n\r\nCuando la crema esté preparada (tras 7minutos), servir en dos tazones o cuencos y echar sobre cada uno 1 cucharada de avena caramelizada, unas 6 ó 7 nueces de pecan y unas gotas de leche de coco (difuminar con la ayuda de un palillo o punta de cuchillo).'),
(47, 'Garbanzos con chocolate', 'Para 4 personas\r\n   - 320 gr. de garbanzos cocidos escurridos\r\n   - 200 gr. de chocolate negro para postres (65% cacao mínimo)\r\n   - 1/2 cucharadita de pimentón de La Vera agridulce\r\n   - Escamas de sal\r\n', '1637091532_garbanzos.jpg', 'Una vez tenemos los garbanzos escurridos, vamos a secarlos en la freidora sin aceite, a 185ºC durante 32 minutos, removiendo el cestillo a intervalos. \r\n\r\nCuando tengas los garbanzos secos, deshaz el chocolate en un cazo a fuego lento. Cuando esté desecho por completo, añade el pimentón y remueve con una cucharilla, hasta integrarlo totalmente. Es obligatorio chupar luego la cucharilla, pero asegúrate de que el chocolate está templado.\r\n\r\nFuera de fuego y con el chocolate líquido pero templado, ve añadiendo los garbanzos por tandas, remueve para que se cubren bien de chocolate, y sácalos con una espumadera para dejarlos secar sobre un pedazo de papel de horno. Añade una lluvia ligera de escamas de sal sobre cada tanda, antes de que el chocolate se seque.\r\n\r\nMételos en el frigorífico unos 10 minutos, y al final, cuando estén totalmente secos y fríos, separa los bloques de garbanzos con las manos para que se puedan comer de uno en uno.'),
(48, 'Aros de cebolla', 'Para 4 personas\r\n    - 3 cebollas dulces, grandes y bien redondas\r\n    - 2 huevos\r\n    - 100 ml. de leche\r\n    - 100 gr. de pan rallado\r\n    - 3/4 cucharadita de sal\r\n    - Aceite de oliva virgen extra (para freír)\r\n\r\nPara la salsa barbacoa casera\r\n    - 3 partes de kétchup\r\n    - 2 partes de crema balsámica de Módena\r\n    - Ajo molido en polvo\r\n\r\n', '1637091858_aros.jpg', 'En primer lugar, para hacer la salsa, mezclamos el kétchup con el vinagre balsámico de Módena en las proporciones que indico, y añadimos pequeñas cantidades de ajo molido para ir probando y dejarlo en el punto deseado.\r\n\r\nPelar las cebollas y cortarlas en aros de un centímetro de grosor, más o menos. Separar los aros de cebolla con cuidado de no romperlos.\r\nEn un plato hondo, batir los huevos y mezclar con la leche. En otro plato, mezclar el pan rallado con la sal. Pasar cada aro de cebolla por huevo, pan rallado, huevo y pan rallado. Doble empanado para conseguir que los aros queden bien crujientes.\r\n\r\nPrecalentamos la freidora a 190ºC, y mientras tanto, rociamos los aros de cebolla con aceite de oliva virgen extra en spray. Colocar los aros de cebolla en el cestillo sin amontonar demasiado unos sobre otros, y programar 10 minutos a 190ºC para hacer los aros de cebolla crujientes en una freidora de aire caliente.\r\n'),
(49, 'Salmón al vapor', 'Para 2 personas\r\n   - 2 filetes de salmón\r\n   - 1 cebolleta finamente picada\r\n   - 10 espárragos verdes\r\n   - 1 naranja\r\n   - 1 limón\r\n   - 4 cdas. de salsa de soja\r\n   - 1 cdta. de jengibre fresco rallado (o ½ cdta. en polvo)\r\n   - Sal y pimienta molida\r\n   - Arroz basmati para acompañar', '1637092037_salmon.jpg', 'Para la salsa mezcla en un bol el zumo de media naranja, zumo de medio limón y la salsa de soja. Salpimienta los filetes de salmón y vierte esta salsa por encima, deja macerar durante 10 minutos. Coge una cazuela o cazo con un diámetro menor al de la vaporera de bambú, ponla al fuego con abundante agua.\r\n\r\nEn las dos bandejas de la vaporera coloca unas rodajas de naranja y de limón cubriendo parte de la base. Encima pon un filete de salmón en cada una, unas rodajas de limón y la cebolleta picada. A un lado añade los espárragos verdes.\r\n\r\nPon la vaporera cerrada sobre la cazuela con el agua hirviendo y cocina al vapor unos 10 minutos, el tiempo dependerá también del grosor de los filetes de salmón. Retira la vaporera y sirve el salmón junto con los espárragos y un poco de arroz basmati.\r\n'),
(50, 'Tatín de melocotón', '8 raciones\r\n   - 1 lámina de masa quebrada casera o preparada\r\n   - 5-6 melocotones maduros\r\n   - 80 g de azúcar\r\n   - 40 g de mantequilla\r\n   - 2 cdas. de agua\r\n   - 1/2 vaina de vainilla o 1/4 cdta. de extracto de vainilla\r\n\r\nPara la masa quebrada casera\r\n  - 250 g de harina\r\n  - 120 g de mantequilla fría\r\n   - 2 cdas. de azúcar glas\r\n   - 1 pizca de sal\r\n   - 1 huevo\r\n   - 2 cdas. de agua fría', '1637092394_tatin.jpg', 'Para la masa quebrada\r\nTamiza la harina en un bol. Añade la mantequilla fría cortada en cubos muy pequeños y mezcla con la harina usando las puntas de los dedos, hasta obtener una especie de migas. Añade el azúcar, una pizca de sal, un huevo batido y el agua fría. Amasa ligeramente hasta conseguir una bola de masa homogénea. Envuélvela con film y deja reposar en la nevera al menos 30 minutos.\r\n\r\nPara la tarta Tatin\r\n Lava y seca los melocotones. Quítales el hueso y córtalos en cuartos. Reserva. Para preparar el caramelo pon en el molde para tarta Tatin el azúcar y la mantequillaa fuego medio y espera a que se funda el azúcar y se derrita la mantequilla, removiendo con una pala de madera. Cuando el caramelo tenga un color dorado añade las semillas de la vaina de vainilla, o el extracto y el agua. Remueve hasta que se integren todos los ingredientes.\r\n\r\nAcomoda los melocotones que has cortado poniendo la parte redonda hacia el caramelo. Empieza a colocarlos alrededor del borde y después ocupando el centro. Pon los trozos bien juntos para que no queden huecos. Estira la masa quebrada con un rodillo sobre una superficie enharinada hasta conseguir un grosor de 4 mm aproximadamente, y con un diámetro ligeramente mayor al del molde. Coloca la masa cubriendo los melocotones, recorta la masa si sobra demasiado y dobla los bordes hacia dentro.\r\n\r\nHaz un pequeño corte en forma de cruz en el centro de la masa para que salga el vapor. Hornea a 180°C con calor arriba y abajo, durante unos 30 minutos, hasta que tengan un bonito color dorado.Deja enfriar fuera del horno unos 5 minutos y desmolda la tarta dándole la vuelta al molde con su plato correspondiente, como si fuera una tortilla de patata. No dejes enfriar más la Tatin o el caramelo se endurecerá demasiado y será difícil sacarla. Sirve la tarta Tatin templada o fría, y si quieres puedes acompañarla con yogur o una bola de helado.\r\n'),
(51, 'Falafel con salsa yogur', 'Para 4 personas\r\n    - 300 g. de garbanzos\r\n    - 1 cebolla\r\n    - 2 dientes de ajo (cantidad al gusto)\r\n    - 2 cucharadas de perejil fresco y 1 cucharada de cilantro fresco\r\n    - 1 cucharada tipo café de comino molido\r\n    - 1 cucharada tipo café de levadura química (5 gramos)\r\n    - Aceite de oliva suave (para freír)\r\n    - Sal y pimienta negra recién molida (al gusto)\r\n    - Salsa de yogur: 1 yogur natural cremoso (tipo griego)\r\n    - 2 cucharadas de aceite de oliva virgen extra\r\n    - 1 diente de ajo muy picado\r\n    - El zumo de medio limón\r\n    - Menta fresca picada, sal y pimienta negra\r\n', '1637264745_falafel.jpg', 'Ponemos los garbanzos en remojo 24 horas antes de realizar la receta. Transcurrido este tiempo los escurrimos y secamos bien para que no queden restos de agua. Introducimos los garbanzos hidratados en el vaso de un robot de cocina o una batidora potente junto con el diente de ajo, la cebolla, las hojas de cilantro y de perejil y el comino molido. Trituramos.\r\n\r\nA continuación añadimos la harina de garbanzos, reservando un par de cucharadas para cubrir la base de una fuente, la levadura química y salpimentamos al gusto. Trituramos de nuevo hasta obtener una mezcla homogénea. Dejamos reposar la masa, en la nevera, durante 30 minutos.\r\n\r\nFormamos pequeñas bolitas del tamaño de una nuez y las colocamos en la fuente con la harina de garbanzos al tiempo que rebozamos ligeramente. Calentamos abundante aceite en una sartén y freímos, a fuego medio-alto, volteando para que se doren por igual por ambos lados. Retiramos a un plato con papel absorbente y dejamos escurrir un par de minutos antes de servir.'),
(52, 'Brócoli al vapor', 'Para 4 personas\r\n- 1 brócoli entero \r\n- 1/2 cucharadita de bicarbonato sódico\r\n- 40 g de anacardos\r\n- 2 cucharadas de tahini o pasta de sésamo\r\n- 4 cucharadas de aceite de oliva virgen extra\r\n- zumo de 1 limón\r\n- 8 cucharadas de agua\r\n- sal\r\n- pimienta blanca molida', '1637349769_brocoli.JPG', 'Separamos los ramilletes del brócoli del tronco y cortamos este último en porciones de un bocado. Lavamos todo y los colocamos en una vaporera. Calentamos un poco de agua en una cazuela, añadimos el bicarbonato (que ayudará a potenciar el verde) y colocamos la vaporera encima. \r\n\r\nCocemos al vapor cinco minutos para que quede al dente. Preparamos el aliño y, para ello, exprimimos el limón y lo mezclamos con el tahini o pasta de sésamo y el aceite de oliva virgen extra. Añadimos agua para aligerar el aliño y removemos, notaréis que se vuelve más pálido, y condimentamos con sal y pimienta blanca molida. \r\n\r\nCortamos los anacardos y los tostamos en una sartén sin aceite. Solo queremos que tomen temperatura y un poco de color, con lo que se volverán más aromáticos y sabrosos. Servimos el brócoli con la salsa por encima y los anacardos tostados para decorar.'),
(53, 'Pollo con limón', 'Para 3 personas\r\n- 3 pechugas de pollo\r\n- hierbas frescas, 2 limones\r\n- 100 ml de aceite de oliva virgen extra\r\n- 100 ml de vino blanco\r\n- sal \r\n- pimienta negra', '1637349905_pechuguitas.JPG', 'Salpimentamos las pechugas y las introducimos en una bolsa grande de congelar con cierre junto con 100 ml de aceite y 100 ml de vino blanco. Picamos las hierbas de nuestro gusto y las introducimos también en la bolsa junto con dos limones en rodajas. Cerramos la bolsa, sacamos el aire y masajeamos para que los limones y las hierbas se distribuyan bien entre las pechugas. \r\n\r\nMarinamos entre dos y cuatro horas dentro de la nevera. Calentamos dos cucharadas de aceite de oliva en una sartén y doramos el pollo, bien escurrido de la marinada, durante siete minutos. Añadimos entonces la marinada, tapamos la sartén y cocemos otros seis o siete minutos. Así nos quedaran bien cocinadas, doradas por fuera y muy jugosas por dentro. '),
(54, 'Calabaza asada con brócoli', 'Para 4 personas\r\n- 400 g de calabaza (aprox.)\r\n- 1 brócoli\r\n- 2 g de chile en copos\r\n- 5 g de hierbas provenzales\r\n- 3 g de hinojo seco\r\n- ajo en polvo al gusto, pimienta negra molida\r\n- 1 limón\r\n- 5 ml de vinagre de Jerez\r\n- 30 ml de aceite de oliva virgen extra\r\n- 50 ml de sidra de manzana o vino blanco\r\n- 100 g de arándanos\r\n- 80 g de almendras\r\n- sal gruesa', '1637350074_calabaza.JPG', 'Pelamos la calabaza, sacamos las semillas y los filamentos y cortamos en cubos de, aproximadamente, el mismo tamaño. Cortamos los ramilletes del brócoli (reservamos el tallo para otra receta). Cortamos porciones que sean más o menos de tamaños parecidos, aprovechando también las hojas. Lavamos y escurrimos con suavidad. \r\n\r\nColocamos la calabaza y el brócoli en una bandeja o fuente de horno y mezclamos con todos los demás ingredientes, ajustando las cantidades al gusto. Recomendamos usar un poco de ralladura de limón y luego un chorro de zumo. Cocemos en horno pre calentado a 200ºC durante unos 30 minutos. Removemos de vez en cuando y añadimos un chorro más de sidra si fuera necesario, a mitad de la cocción. \r\n\r\nCuando falten unos 10 minutos, tostamos las almendras en otra fuente, vigilando bien que no se quemen. Servimos mezclando una porción con almendras, arándanos frescos lavados y un chorrito de aceite de oliva virgen extra, sal gruesa y más hierbas al gusto.'),
(55, 'Espirales de calabacín', '- 2 calabacines pequeños\r\n- 2 dientes de ajo\r\n- 15 ml de aceite de oliva virgen extra\r\n- 10 ml de mostaza de Dijon\r\n- 5 ml de vinagre de manzana o Jerez\r\n- 5 ml de zumo de limón\r\n- 20 ml de tahini, miel (opcional)\r\n- queso Parmesano o similar al gusto\r\n- semillas de sésamo tostadas\r\n- pimienta negra molida\r\n- sal\r\n- orégano seco o fresco al gusto', '1637350212_calabacin.JPG', 'Lavar y secar bien los calabacines. Sacar los tallarines usando un espiralizador, una mandolina, un cortador manual o un pelador con dientes. Con estos últimos métodos habrá que ir girando el calabacín para sacar las tiras antes de llegar al corazón, más duro y con semillas, que podemos reservar para una crema o sopa. \r\n\r\nDisponer en un colador, echar una buena pizca de sal y remover. Dejar que escurra un poco el agua durante unos 15 minutos. Mientras, preparar la salsa batiendo en un cuenco todos los ingredientes. Probar y ajustar las cantidades al gusto.'),
(56, 'Escalivada', 'Para 4 personas\r\n    - Berenjena 1\r\n    - Pimiento rojo 1\r\n    - Cebolla 2\r\n    - Cabeza de ajos 1\r\n    - Tomate 3\r\n    - Aceite de oliva virgen extra\r\n    - Sal gruesa\r\n', '1637350416_escalivada.jpg', 'Lavamos y secamos la berenjena, el pimiento y los tomates. Retiramos la capa exterior de las cebollas y de la cabeza de ajos. Pinchamos la berenjena con un cuchillo afilado para que no reviente con el calor del horno.\r\n\r\nColocamos todas las verduras en una bandeja para horno, excepto los tomates, y las regamos con un poco de aceite de oliva virgen extra. Introducimos la bandeja en el horno precalentado a 170ºC durante una hora. Volteamos las verduras, incorporamos los tomates a la bandeja y asamos 30 minutos más.\r\n\r\nDependiendo del tamaño de las verduras, especialmente de las cebollas, el tiempo puede necesitar ajustes así que el indicado es meramente orientativo. Comprobamos que estén tiernas las verduras antes de retirar la bandeja del horno y dejamos enfriar para poder quitar la piel y pepitas al pimiento y a la berenjena.\r\n\r\nQuitamos las capas exteriores de las cebollas y las troceamos. También sacamos, presionando ligeramente, los dientes de ajo de la cabeza asada. Vamos disponiendo todos los ingredientes en un plato, al que añadiremos una pizca de sal gruesa marina y los jugos del asado. Hay quien también le añade unas gotas de vinagre.'),
(57, 'Ensalada payesa ', 'Para 6 personas\r\n    - Patata 6\r\n    - Tomate 3\r\n    - Cebolla 0.5\r\n    - Pimiento verde 0.5\r\n    - Atún en aceite de oliva, lata 3\r\n    - Aceitunas rellenas, lata pequeña 2\r\n    - Aceite de oliva virgen extra\r\n    - Sal\r\n', '1637350561_ensalada.jpg', 'Empezamos lavando las patatas y poniéndolas en una olla tapadas con agua fría. Ponemos la olla al fuego y la dejamos hervir hasta que pinchándolas con una aguja esta pueda penetrar con facilidad en la carne de cada patata. Aproximadamente una media hora.\r\n\r\nUna vez cocidas, las sacamos del agua y las dejamos enfriar sobre un plato. Mientras, cortamos los tomates y el pimiento verde en cuadraditos y cortamos la cebolla en lonchas finas. Escurrimos el atún y lo desmenuzamos con la ayuda de un tenedor.\r\n\r\nPelamos las patatas y las cortamos a cubos. Las ponemos en la ensaladera donde vayamos a servir el plato y añadimos los ingredientes que teníamos reservados. Escurrimos las aceitunas, las cortamos por la mitad y las añadimos a la ensalada, removiendo bien. Por último, aliñamos con abundante aceite de oliva y sal.'),
(58, 'Tortilla freidora de aire', 'Para 6 personas\r\n    - 500 g de patatas\r\n    - 6 huevos\r\n    - 1 cebolla\r\n    - sal\r\n    - 1 cucharada de aceite de oliva virgen extra\r\n', '1637350918_tortilla.JPG', 'Pelamos y troceamos las patatas y la cebolla en rodajas finas de 5 mm. Ponemos las patatas en la cesta de la freidora, salamos, añadimos el aceite Programamos 30 minutos a 180°C* (precalentado). Cuando hayan pasado 20 minutos incorporamos las cebollas. Removemos de vez en cuando para que se cocinen parejo. \r\n\r\nPasado este tiempo controlamos que patatas y cebolla estén cocidas y añadimos el huevo batido y removemos para mezclar. Programamos 6-8 minutos a 130°C para cuajar la tortilla pero que quede jugosa.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nick` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido1` varchar(255) NOT NULL,
  `apellido2` varchar(255) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `edad` int(11) NOT NULL,
  `telefono` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nick`, `nombre`, `apellido1`, `apellido2`, `contrasena`, `edad`, `telefono`, `email`) VALUES
(34, 'Mateo', 'mateo', 'barcelona', 'rodriguez', '123', 24, 600909090, 'hola@hola.com'),
(36, 'Kikote', 'kike', 'Muñoz', 'To', '1234', 28, 600090909, 'prueba@hola.com'),
(37, 'SaraM', 'Sara', 'Mallorca', 'Rodriguez', 'hola', 20, 600504030, 'hola@hola.com'),
(38, 'Fede', 'Federico', 'Pinilla', 'Solans', '1234', 45, 600909090, 'prueba@hola.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id_comentario`);

--
-- Indices de la tabla `recetas`
--
ALTER TABLE `recetas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id_comentario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `recetas`
--
ALTER TABLE `recetas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
