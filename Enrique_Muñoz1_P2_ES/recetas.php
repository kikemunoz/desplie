<link rel="stylesheet" href="./css/css.css" />
<?php include("template/cabecera.php"); ?>

<?php 
include ("admin/config/bd.php");
$sentenciaSQL=$conexion->prepare("SELECT * FROM recetas");
$sentenciaSQL->execute();
$listaRecetas=$sentenciaSQL->fetchAll(PDO::FETCH_ASSOC);
?>


<?php foreach($listaRecetas as $receta) { ?>
<div class="col-md-3">    
<div class="card">
<img width="auto"   class='imgRedonda' src="./img/<?php echo $receta['imagen']; ?>" alt="">
<div class="card-body">
    <h4 class="card-title" style="text-align: center;"><?php echo $receta['nombre']; ?></h4> 
    <div style="text-align: center;">
    <a class="btn btn-primary" href="recetaIn.php?id=<?=$receta['id'];?>" role="button">Ver más</a>
</div>
</div>
</div>
<br/>
</div>

<?php } ?>

</p>
<?php include("template/pie.php"); ?>